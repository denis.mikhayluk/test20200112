<?php

declare(strict_types=1);

namespace src\courses;

class CoursesCommon {
	private $methods = ['cache','db','bank'];

	public function __construct ($methods = false) {
		if ($methods) $this->methods = $methods;
	}

	public function get_info (): object {

		$dataCourses = [];
		$time = time();
		foreach ($this->methods as $method) {
			$className = 'src\courses\store\Courses'.ucfirst($method);
			$classData = $className::getData($time);

			if ($classData && $classData->status) {
				$dataCourses = $classData->data;
				$className::setData();
				break;
			}
		}

		return (object) [
			'status' => COUNT($dataCourses) ? true : false,
			'methodName' => $className,
			'method' => $method,
			'data' => $dataCourses
		];

	}
}
