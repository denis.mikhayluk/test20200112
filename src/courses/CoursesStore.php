<?php

declare(strict_types=1);

namespace src\courses;

interface CoursesStore
{
    public static function getData($time): object;

    public static function setData(): object;
}