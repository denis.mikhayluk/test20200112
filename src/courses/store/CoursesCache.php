<?php

declare(strict_types=1);

namespace src\courses\store;

use src\courses\CoursesStore;
use src\store\ParseCacheJson;

class CoursesCache implements CoursesStore {

	public static $path = 'resources/courses.txt';

	public static function getData($time): object {

		$data = ParseCacheJson::get(self::$path);

		return (object) [
			'status' => ($data->status && ($time - 5) < $data->time ? true : false),
			'data' => $data->data
		];
	}

	public static function setData(): object {
		return (object) [
			'status' => true
		];
	}

}
