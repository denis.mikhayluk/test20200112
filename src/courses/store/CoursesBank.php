<?php

declare(strict_types=1);

namespace src\courses\store;

use src\courses\CoursesStore;
use src\courses\store\CoursesCache;
use src\store\ParseUrlJson;
use src\store\ParseDbJson;
use src\store\ParseCacheJson;

class CoursesBank implements CoursesStore {

	private static $data = [];

	public static function getData($time): object {

		$data = ParseUrlJson::get('http');
		self::$data = $data->data;

		return (object) [
			'status' => true,
			'data' => self::$data
		];
	}

	public static function setData(): object {
		ParseDbJson::save(self::$data);
		ParseCacheJson::save(CoursesCache::$path, self::$data);

		return (object) [
			'status' => true
		];
	}

}