<?php

declare(strict_types=1);

namespace src\courses\store;

use src\courses\CoursesStore;
use src\courses\store\CoursesCache;
use src\store\ParseDbJson;
use src\store\ParseCacheJson;

class CoursesDb implements CoursesStore {

	private static $data = [];

	public static function getData($time): object {

		$data = ParseDbJson::get('courses');
		self::$data = $data->data;

		return (object) [
			'status' => ($data->status && ($time - 60) < $data->time ? true : false),
			'data' => self::$data
		];
	}

	public static function setData(): object {
		ParseCacheJson::save(CoursesCache::$path, self::$data);

		return (object) [
			'status' => true
		];
	}
}