<?php

declare(strict_types=1);

namespace src\store;

class ParseDbJson {

	public static function get($path): object {
		
		$time = time() - rand(1, 120);
		$data = [
			'USD' => rand(1000, 4999)/100,
			'EUR' => rand(5000, 9999)/100,
		];
		return (object) [
			'status' => (rand(0, 1) ? true : false),
			'time' => $time,
			'data' => $data
		];
	}

	public static function save($data): bool {
		return true;
	}

}
