<?php

declare(strict_types=1);

namespace src\store;

class ParseUrlJson {

	public static function get($path): object {

		$time = time();

		$data = [
			'USD' => rand(10000, 49999)/100,
			'EUR' => rand(50000, 99999)/100,
		];
		return (object) [
			'status' => true,
			'time' => $time,
			'data' => $data
		];
	}

	public static function save($data): bool {
		return true;
	}

}
