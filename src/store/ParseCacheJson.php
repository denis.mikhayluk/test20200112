<?php

declare(strict_types=1);

namespace src\store;

class ParseCacheJson {

	public static function get($file): object {

		if (file_exists($file)) {
			$cache = file_get_contents($file);
			return (object) [
				'status' => true,
				'time' => filemtime($file),
				'data' => json_decode($cache, true)
			];
		} else {
			return (object) [
				'status' => false,
				'time' => 0,
				'data' => []
			];
		}
	}

	public static function save($file, $data): bool {

		$fp = fopen($file, 'w');
		fwrite($fp, json_encode($data));
		fclose($fp);

		return true;
	}

}
